#include <stdio.h>

//Task 1.1
//Global variables, used to find their addresses
int globalInt = 10;
char globalChar[] = "hello";
float globalFloat = 10.2;
double globalD = 7.5;

char globalCharUnInit;
int globalIntUnInit;
float globalFloatUnInit;
double globalDUnInit;


//Calculates the sum of the numbers 1 to n
int sum_n(int n) {
    //printf("Address: %p\n", &n);
    if (n != 0) {
        return n + sum_n(n-1);
    }
    else
        return n;
}

int main() {
    printf("Sum & recursion \n");
    int n = 5;
    sum_n(n);
    printf("The sum of numbers 1 to %d is %d.\n\n", n, sum_n(n));

    //Local variables, used to find their addresses
    int localInt = 10;
    char localChar[] = "welcome";
    float localFloat = 3.5;
    double localD = 1.4;
    char localCharUnInit;
    int localIntUnInit;

    printf("\nAddresses of different variables, both local and global\n");
	printf("Address of globalInt is %p\n", &globalInt);
	printf("Address of globalChar is %p\n", &globalChar);
	printf("Address of globalFloat is %p\n", &globalFloat);
	printf("Address of globalD is %p\n", &globalD);
	printf("Address of globalIntUnInit is %p\n", &globalIntUnInit);
	printf("Address of globalCharUnInit is %p\n", &globalCharUnInit);
	printf("Address of globalFloatUnInit is %p\n", &globalFloatUnInit);
	printf("Address of globalDUnInit is %p\n", &globalDUnInit);
	printf("Address of localInt is %p\n", &localInt);
	printf("Address of localChar is %p\n", &localChar);
	printf("Address of localFloat is %p\n", &localFloat);
	printf("Address of localDouble is %p\n", &localD);
	printf("Address of localIntUnInit is %p\n", &localIntUnInit);
    printf("Address of localCharUnInit is %p\n", &localCharUnInit);

    //Comparison of local addresses
    int integer = 100;
    int integer2 = 20;
    int integer3 = 67;

    char character[] = "k";
    char character2[] = "abcde";

    int undefined;
    int undefined2;
    int undefined3;

    printf("\n\nAddresses of local initialised vs uninitialised variables\n");
    printf("Address of integer is %p\n", &integer);
    printf("Address of integer2 is %p\n", &integer2);
    printf("Address of integer3 is %p\n", &integer3);

    printf("Address of character is %p\n", &character);
    printf("Address of character2 is %p\n", &character2);

    printf("Address of undefined is %p\n", &undefined);
    printf("Address of undefined2 is %p\n", &undefined2);
    printf("Address of undefined3 is %p\n", &undefined3);

	return 0;
}